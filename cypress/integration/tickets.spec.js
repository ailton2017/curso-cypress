describe("Tickets", () => {
    beforeEach( () => cy.visit("https://ticket-box.s3.eu-central-1.amazonaws.com/index.html"));
   
   it("fills all text input fields", () => {
     const firstName = "ailton";
     const lastName = "cabral"; 
   
     cy.get("#first-name").type(firstName);
     cy.get("#last-name").type(lastName);
     cy.get("#email").type("ailton_csf@hotmail.com");
     cy.get("#requests").type("vegetarian");
     cy.get("#signature").type(`${firstName} ${lastName}`);
});
   
   it("select two tickets", () => {
     cy.get("#ticket-quantity").select("2");
   });
  
  
   it("select 'vip' ticket type", () => {
      cy.get("#vip").check(); 
  });

   it("select 'friend', and 'publication', then uncheck 'friend'", () => {
     cy.get("#friend").check();
     cy.get("#publication").check();
     cy.get("#friend").uncheck();
   });
 
   it("has 'TICKETBOX' header's heading ", () => {
     cy.get("header h1").should("contain", "TICKETBOX");
});
   it("alerts on invalid email", () => {
    cy.get("#email")
      .as("email")
      .type("ailton-gmail.com");
    
      cy.get("#email.invalid").should("exist");

      cy.get("@email")
        .clear()
        .type("ailton_csf@hotmail.com");

      cy.get("#email.invalid").should("not.exist");
      });
  
    it("fills and reset the form", () => {
        const firstName = "ailton";
        const lastName = "cabral";
        const fullName = `${firstName} ${lastName}`;

        cy.get("#first-name").type(firstName);
        cy.get("#last-name").type(lastName);
        cy.get("#email").type("ailton_csf@hotmail.com");
        cy.get("#ticket-quantity").select("2");
        cy.get("#vip").check();
        cy.get("#friend").check();
        cy.get("#requests").type("luta Brasil");

        cy.get(".agreement p").should(
           "contain",
           `I, ${fullName}, wish to buy 2 VIP tickets.`
        );
    
        cy.get("#agree").click();
        cy.get("#signature").type(fullName);


        cy.get("button[type='submit']")
          .as("submitButton")
          .should("not.be.disabled");
        
        
        cy.get("button[type='reset']").click();

        cy.get("@submitButton").should("be.disabled");

      });

    it("fills mandatory fields using support command", () => {
      const customer = {
        firstName: "ailton",
        lastName: "cabral",
        email: "ailton_csf@hotmail.com"
      };


      cy.fillMandatoryFields(customer);

      cy.get("button[type='submit']")
        .as("submitButton")
        .should("not.be.disabled");

      
      cy.get("button[type='reset']").click();

      cy.get("@submitButton").should("be.disabled");
    
    
    });

});